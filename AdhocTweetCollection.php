<?php

namespace App\Models\Adhoc;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AdhocTweetCollection extends Eloquent
{
    // adjust collection name in mongodb
	protected $connection = 'mongodb4';
	protected $collection = 'captions';
    protected $guarded = [];

}
