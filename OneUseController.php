<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Table\AccountCampaignActivity;
use App\Models\Table\AccountTable;
use App\Models\Table\AutoPost;
use App\Models\Table\HomePost;
use App\Models\Table\Interest;
use App\Services\AccountData;
use App\Services\AccountLastStatus;
use Illuminate\Http\Request;
use DataTables;

use App\Models\Table\JobTable;
use App\Services\JobTranslation;

class OneUseController extends Controller
{
    public function update_account_page()
    {
        return view('backsite.one_use.update_account');
    }

    public function bulk_update_page()
    {
        return view('backsite.one_use.bulk_update_account');
    }

    public function update_account(Request $request)
    {
        switch ($request->input('update_account')) {
            case 'update_status':
                $accounts = AccountTable::all();

                foreach ($accounts as $account) {
                    $status = AccountLastStatus::status($account);
                    $account->update(['active' => $status]);
                }

                return back()->with('success', 'Account status has been updated');
                break;
            case 'update_data':
                $accounts = AccountTable::all();

                foreach ($accounts as $account) {
                    $data = new AccountData();
                    $data->generate($account);
                }

                return back()->with('success', 'Account statistics has been updated');
                break;
            default:
                break;
        }
    }

    public function set_active_day(Request $request)
    {
        $accounts = AccountTable::whereIn('id', json_decode($request->account_id))->get();

        foreach ($accounts as $account) {
            $account->update([
                'start_active_hour' => $request->start_hour,
                'end_active_hour' => $request->end_hour,
                'active_day' => isset($request->active_day) ? json_encode($request->active_day) : []
            ]);
        }

        return redirect(url('/backsite/one-use/bulk-update'))->with('success', 'Account active day has been updated');
    }

    public function set_campaign_post(Request $request)
    {
        $accounts = AccountTable::whereIn('id', json_decode($request->account_id))->get();
        $activity = [];

        if ($request->platform == 'twitter') {
            $activity = [
                'tweet' => intval($request->tweet),
                'retweet' => intval($request->retweet),
                'like' => intval($request->like),
                'reply' => intval($request->reply),
                'quote' => intval($request->quote)
            ];
        } else {
            $activity = [
                'like' => intval($request->like),
                'comment' => intval($request->comment)
            ];
        }

        foreach ($accounts as $account) {
            AccountCampaignActivity::updateOrCreate(
                ['account_id' => $account->id],
                ['activity' => json_encode($activity)]
            );
        }

        return redirect(url('/backsite/one-use/bulk-update'))->with('success', 'Account campaign activities has been updated');
    }

    public function set_auto_post(Request $request)
    {
        $accounts = AccountTable::whereIn('id', json_decode($request->account_id))->get();

        foreach ($accounts as $account) {
            AutoPost::updateOrCreate(
                ['account_id' => $account->id],
                ['count' => intval($request->count) ?? 0]
            );
        }

        return redirect(url('/backsite/one-use/bulk-update'))->with('success', 'Account auto posts has been updated');
    }

    public function set_home_post(Request $request)
    {
        $accounts = AccountTable::whereIn('id', json_decode($request->account_id))->get();

        foreach ($accounts as $account) {
            HomePost::updateOrCreate(
                ['account_id' => $account->id],
                [
                    'action' => isset($request->action) ? json_encode($request->action) : [],
                    'count' => intval($request->count) ?? 0,
                    'follow' => intval($request->follow) ?? 0
                ]
            );
        }

        return redirect(url('/backsite/one-use/bulk-update'))->with('success', 'Account home posts has been updated');
    }

    public function set_interaction(Request $request)
    {
        $accounts = AccountTable::whereIn('id', json_decode($request->account_id))->get();

        foreach ($accounts as $account) {
            Interest::updateOrCreate(
                ['account_id' => $account->id],
                [
                    'username' => isset($request->username) ? json_encode(array_map('trim', explode(',', $request->username))) : [],
                    'action' => isset($request->action) ? json_encode($request->action) : [],
                    'count' => intval($request->count) ?? 0
                ]
            );
        }

        return redirect(url('/backsite/one-use/bulk-update'))->with('success', 'Account interaction has been updated');
    }

    public function set_all(Request $request)
    {
        $accounts = AccountTable::whereIn('id', json_decode($request->account_id))->get();
        $activity = [];

        if ($request->platform == 'twitter') {
            $activity = [
                'tweet' => intval($request->tweet),
                'retweet' => intval($request->retweet),
                'like' => intval($request->like),
                'reply' => intval($request->reply),
                'quote' => intval($request->quote)
            ];
        } else {
            $activity = [
                'post' => intval($request->post),
                'like' => intval($request->like),
                'comment' => intval($request->comment)
            ];
        }

        foreach ($accounts as $account) {
            $account->update([
                'start_active_hour' => $request->start_hour,
                'end_active_hour' => $request->end_hour,
                'active_day' => isset($request->active_day) ? json_encode($request->active_day) : []
            ]);

            AccountCampaignActivity::updateOrCreate(
                ['account_id' => $account->id],
                ['activity' => json_encode($activity)]
            );

            AutoPost::updateOrCreate(
                ['account_id' => $account->id],
                ['count' => intval($request->count_auto_post) ?? 0]
            );

            HomePost::updateOrCreate(
                ['account_id' => $account->id],
                [
                    'action' => isset($request->action) ? json_encode($request->action) : [],
                    'count' => intval($request->count) ?? 0,
                    'follow' => intval($request->follow) ?? 0
                ]
            );
        }

        return redirect(url('/backsite/one-use/bulk-update'))->with('success', 'Account data has been updated');
    }

    public function feature(Request $request)
    {
        switch ($request->input('bulk_update')) {
            case 'active_day':
                return view('backsite.one_use.set_active_day', [
                    'account_id' => $request->id,
                    'platform' => $request->platform,
                    'status' => $request->status
                ]);
                break;
            case 'auto_post':
                return view('backsite.one_use.set_auto_post', [
                    'account_id' => $request->id,
                    'platform' => $request->platform,
                    'status' => $request->status
                ]);
                break;
            case 'home_post':
                return view('backsite.one_use.set_home_post', [
                    'account_id' => $request->id,
                    'platform' => $request->platform,
                    'status' => $request->status
                ]);
                break;
            case 'campaign_post':
                return view('backsite.one_use.set_campaign_post', [
                    'account_id' => $request->id,
                    'platform' => $request->platform,
                    'status' => $request->status
                ]);
                break;
            case 'interaction':
                return view('backsite.one_use.set_interaction', [
                    'account_id' => $request->id,
                    'platform' => $request->platform,
                    'status' => $request->status
                ]);
                break;
            default:
                return view('backsite.one_use.set_all', [
                    'account_id' => $request->id,
                    'platform' => $request->platform,
                    'status' => $request->status
                ]);
                break;
        }
    }

    public function data(Request $request)
    {
        $data = AccountTable::where('is_assigned', 1);

        if ($request->platform) {
            $data = $data->where('platform', $request->platform);
        }

        if ($request->status) {
            if ($request->status === 'active') {
                $data = $data->where('active', '1');
            }

            if ($request->status === 'inactive') {
                $data = $data->where('active', '0');
            }
        }

        return datatables()->of($data->get())->addColumn('active_hour', function ($data) {
            $active_hour = "{$data->start_active_hour} to {$data->end_active_hour}";
            return $active_hour;
        })->addColumn('checkbox', function ($data) {
            $checkbox = "<input type='checkbox' class='child-checkbox' name='id[]' value={$data->id}>";
            return $checkbox;
        })->rawColumns(['active_hour', 'checkbox'])->make(true);
    }

    public function testing()
    {
        $parentJob = JobTable::where('execution_time', '<=', '2022-01-16')->whereNull('parent_job_id')->get();
        JobTranslation::translate($parentJob);
    }
}
