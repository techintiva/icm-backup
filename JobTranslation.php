<?php

namespace App\Services;

use App\Models\Collection\JobCollection;
use App\Models\Table\AccountTable;
use App\Models\Table\JobTable;

class JobTranslation
{
    public static function translate($jobs)
    {
        foreach ($jobs as $job) {
            $acc = $job->account;

            if (!$acc) {
                $acc = AccountTable::where('username', $job->username)
                    ->where('is_assigned', 1)->first();
            }

            $input = self::setJob($job, $acc);
            $jobCollection = JobCollection::create($input);

            $child = JobTable::where('parent_job_id', $job->id)->update(['parent_job_id' => $jobCollection->_id]);

            if ($job->relatable_type === 'App\Models\Table\Post' || $job->relatable_type === 'App\Models\Table\MassReport') {
                $relatable = $job->relatable_type::find($job->relatable_id);
                $ids = $relatable->job_collection_id ?? [];
                array_push($ids, $jobCollection->_id);
                $relatable->job_collection_id = json_encode($ids);
                $relatable->save();
            }

            $job->delete();
        }
    }

    //androi: name, ip, port
    //web: name, ip

    private static function setJob($job, $acc = null): array
    {
        $input = [];

        /*$input['proxy'] = [
            'http' => $acc ? 'http://'.$acc->proxy : null,
            'https' => $acc ? 'https://'.$acc->proxy : null,
        ];*/
        $input['proxy'] = $acc ? $acc->proxy : null;

        if ($job->parent_job_id) {
            $input['parent_id'] = $job->parent_job_id;
        }

        if ($acc) {
            $input['platform'] = $acc->platform;

            switch ($acc->platform) {
                case "twitter":
                    $input['bot'] = [
                        'id' => $acc->id,
                        'username' => $acc->username,
                        'password' => $acc->password,
                        'phone' => $acc->phone_number,
                    ];
                    break;
                case "instagram":
                    $input['bot'] = [
                        'username' => $acc->username,
                        'password' => $acc->password,
                    ];
                    break;
                case "facebook":
                    $input['bot'] = [
                        'username' => $acc->username,
                        'password' => $acc->password,
                    ];
                    break;
            }

            $input['channel'] = [
                'name' => $acc->channel,
                'ip' => $acc->ip,
            ];

            if ($acc->port != '' || $acc->port != null) {
                $input['channel']['port'] = $acc->port;
            }
        }

        if ($job->action === "crawler") {
            $input['channel'] = [
                'name' => 'crawler-server-1',
                'ip' => '15.15.15.118',
            ];
        }

        $input['action'] = $job->action;
        $input['detail'] = $job->detail;

        return $input;
    }
}
