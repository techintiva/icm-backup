<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Campaign Report ICM</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.6.1/mapbox-gl.css' rel='stylesheet' />
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.6.1/mapbox-gl.js'></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://d3js.org/d3.v4.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/holtzy/D3-graph-gallery@master/LIB/d3.layout.cloud.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/locale/id.min.js" integrity="sha512-he8U4ic6kf3kustvJfiERUpojM8barHoz0WYpAUDWQVn61efpm3aVAD8RWL8OloaDDzMZ1gZiubF9OSdYBqHfQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js" integrity="sha512-BNaRQnYJYiPSqHHDb58B0yaPfCu+Wgds8Gp/gU33kqBtgNS4tSPHuGibyoeqMV/TJlSKda6FXzoEyYGjTe+vXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.0/jspdf.umd.min.js" integrity="sha512-5yTVoG0jFRsDhgYEoKrZCj5Bazxqa0VnETLN7k0SazQcARBsbgrSb6um+YpzWKNKV2kjb8bhna4fDfOk3YPr4Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
    <style>
        * {
            font-family: sans-serif;
        }

        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 1122px;
            font-size: 13px;
        }

        .float-right {
            float: right;
        }

        .body-pdf {
            background-color: #FFFFFF;
        }

        .body-pdf p {
            padding: 0;
            margin: 0;
            color: #FFFFFF
        }

        .graph-box {
            border-radius: 8px;
            padding: 30px;
            background-color: rgba(0, 0, 0, 0.2);
        }

        .marker-yellow {
            background-image: url("/img/marker-yellow.png");
            background-size: cover;
            width: 40px;
            height: 40px;
            border-radius: 50%;
            cursor: pointer;
        }

        .marker-red {
            background-image: url("/img/marker-red.png");
            background-size: cover;
            width: 40px;
            height: 40px;
            border-radius: 50%;
            cursor: pointer;
        }

        .marker-blue {
            background-image: url("/img/marker-blue.png");
            background-size: cover;
            width: 40px;
            height: 40px;
            border-radius: 50%;
            cursor: pointer;
        }

        .tweet-container {
            float: left;
            width: 25%;
            padding: 10px 0;
        }

        .tweet-box {
            width: 90%;
            background-color: #1B1D2C;
            border-radius: 4px;
            padding: 10px;
            height: 190px;
        }

        .avatar {
            height: 40px;
            width: 40px;
            border-radius: 50%;
        }

        .page-bg {
            background-color: #FAFAFA;
            background-image: url("http://15.15.15.118:8090/img/background-pdf.png");
            /* Full height */
            height: 100%;
            background-repeat: no-repeat;
            background-size: 100% 100%;
            background-position: center;
        }

        .column {
            float: left;
            width: 50%;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        .page-bg{
            background-color: #FAFAFA;
            background-image: url("http://15.15.15.118:8090/img/background-pdf.png");
            /* Full height */
            height: 100%;
            background-repeat: no-repeat;
            background-size: 100% 100%;
            background-position: center;
        }


        @page { size: 793px 1122px landscape; }

    </style>
</head>

<body width="100%" id="capture">
    <div class="body-pdf">
        <div style="height:793px;" class="page-bg">
            <div style="padding-top:400px;padding-left:100px">
                <p style="font-size:30px;margin-bottom:10px">{{$campaign->date_start->format("d F Y")}} -
                    {{$campaign->date_finish->format("d F Y")}}</p>
                <p style="font-size:40px;font-weight:700">Laporan Post</p>
            </div>
        </div>
        <div style="height:793px;position: relative;" class="page-bg">
            <div style="margin:auto;position: absolute;top:40%;width:100%;text-align:center">
                <table style="width: 100%">
                    <tr>
                        <td style="padding: 0 20px;text-align:right;width:35%">
                            @if(in_array('twitter', $campaign->platform))
                            <img height="80px" width="80px" src="http://15.15.15.118:8090/img/ic_twitter.png">
                            @endif
                            @if(in_array('facebook', $campaign->platform))
                            <img height="80px" width="100px" src="http://15.15.15.118:8090/img/ic_facebook.png">
                            @endif
                            @if(in_array('instagram', $campaign->platform))
                            <img height="80px" width="80px" src="http://15.15.15.118:8090/img/ic_instagram.png">
                            @endif
                        </td>
                        <td style="padding: 0 20px;text-align:left;width:65%">
                            @foreach ($campaign->platform as $platform)
                            <p style="font-size:18px;margin-bottom:5px;font-weight:600;">{{strtoupper($platform)}} </p>
                            @endforeach
                            <p style="font-size:22px;margin-bottom:5px;font-weight:600">
                                {{(in_array('twitter', $campaign->platform) ? $account['twitter'] : 0)
                                + (in_array('facebook', $campaign->platform) ? $account['facebook'] : 0)
                                + (in_array('instagram', $campaign->platform) ? $account['instagram'] : 0)}}
                                Robot</p>
                            @if(in_array('twitter', $campaign->platform))
                            <p style="font-size:22px;margin-bottom:5px;font-weight:600">
                                {{$counts['total_tweet'] + $counts['total_tweet'] + $counts['total_tweet']}}
                                Tweet ({{$counts['total_tweet']}} Tweet + {{$counts['total_reply']}} Reply +
                                {{$counts['total_quote']}} Quote)</p>
                            @endif
                            @if(in_array('facebook', $campaign->platform))
                            <p style="font-size:22px;margin-bottom:5px;font-weight:600">
                                {{$counts['total_post_fb']}}
                                Post FB </p>
                            @endif
                            @if(in_array('instagram', $campaign->platform))
                            <p style="font-size:22px;margin-bottom:5px;font-weight:600">
                                {{$counts['total_post_ig']}}
                                Post IG </p>
                            @endif

                            <p style="font-size:22px;margin-bottom:5px;font-weight:600">{{$data->analytics->reach}} Estimated
                                Reaches</p>
                            <p style="font-size:22px;margin-bottom:5px;font-weight:600">{{$data->analytics->impression}} Total
                                Impressions</p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="height:793px; padding: 20px;" class="page-bg">
            <div style="margin:auto">
                <p style="font-size:36px;font-weight:600;margin:40px;margin-bottom:80px">Peta Sebaran</p>
                <div id='map' style='width: 100%; height: 500px;' ></div>
            </div>
        </div>
        <div style="height:793px; padding: 20px;" class="page-bg">
            <p style="font-size:36px;font-weight:600;margin:40px">
                "
                @for ($i=0;$i<count($campaign->keyword);$i++)
                    {{$campaign->keyword[$i]}} {{$i<count($campaign->keyword) - 1 ? ", " : ""}}
                    @endfor
                    "
                    - Mention
            </p>
            <div style="margin-top:120px">
                <canvas id="myChart" height="450"></canvas>
            </div>
        </div>
        <div style="height:793px; padding: 20px;position: relative;" class="page-bg">
            <p style="font-size:36px;font-weight:600;margin:40px">
                "
                @for ($i=0;$i<count($campaign->keyword);$i++)
                    {{$campaign->keyword[$i]}} {{$i<count($campaign->keyword) - 1 ? ", " : ""}}
                    @endfor
                    "
                    - Topic & Sentiment
            </p>
            <div class="graph-box" style="width: 800px;">
                <div id="my_dataviz" style="background-color:#FFFFFF;"></div>
            </div>
            <div style="width: 400px;position: absolute;right:40px;
                bottom:40px;border:rgba(0,0,0,0.2) solid 1px; border-radius:2px;
                display:flex;justify-content:center; padding:60px 30px;background-color:#FFFFFF">
                <div style="margin: 20px">
                    <p style="font-size:44px;color:rgb(80, 227, 194)!important">{{$data->analytics->sentiment->positive}}</p>
                    <p style="font-size:24px;color:rgb(80, 227, 194)!important">POSITIVE</p>
                </div>
                <div style="margin: 20px;">
                    <p style="font-size:44px;color:rgb(227, 80, 122)!important">{{$data->analytics->sentiment->negative}}</p>
                    <p style="font-size:24px;color:rgb(227, 80, 122)!important">NEGATIVE</p>
                </div>
            </div>
        </div>
        <div style="height:753px; padding: 20px;" class="page-bg">
            <p style="font-size:36px;font-weight:600;margin:40px">Engagement - Conversations
            </p>
            <div class="graph-box" style="height: 550px">
                <p style="color: #FFFFFF;font-size:24px">*Most Engaging dari akun robot</p>
                <div style="margin-top:40px;width:100%">
                    @for ($i=0;$i<8;$i++)
                        @php
                        $post=$data->analytics->posts->bot[$i]
                        @endphp
                        @if ($i%4 === 0)
                        <div style="width: 100%;
                        display: table;
                        clear: both;">
                        @endif
                        <div class="tweet-container">
                            <div class="tweet-box">
                                <table style="margin-bottom:10px">
                                    <tr style="vertical-align:top">
                                        <td><img class="avatar" src={{$post->profile_image_url}}
                                                onerror="reloadImage(this)"
                                                style="margin-right:10px"></td>
                                        <td>
                                            <div>
                                                <p style="font-style: normal;
                                        font-weight: 600;
                                        font-size: 12px;
                                        line-height: 16px;
                                        color: #B1AFCD;">{{ucwords($post->name)}}</p>
                                                <p style="font-size: 12px;
                                        line-height: 16px;
                                        color:#D18300;">{{'@'.$post->username}}</p>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="margin-left: auto;text-align:right">
                                                <p style="font-size: 12px;
                                            line-height: 14px;
                                            color: #B1AFCD;">Engagement</p>
                                                <p style="font-size: 14px;
                                            line-height: 14px;">{{$post->engagements}}</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div style="margin-bottom:15px;">
                                    <p style="font-size: 14px;
                                    line-height: 16px;
                                    color: #B1AFCD;
                                    margin-bottom:10px;">
                                        {{strlen($post->text) > 60 ? substr($post->text,0,60)."..." : $post->text;}}</p>
                                    <p style="font-size: 14px;
                                    line-height: 16px;
                                    color: #888888;">{{\Carbon\Carbon::parse($post->created_at)->format("H:i:s, d F Y")}}
                                        </p>
                                </div>
                                <table>
                                    <tr>
                                        <td style="width:30%">
                                            <div style="padding:0 20px 0 0">
                                                <p style="color: #50E3C2;margin-bottom:5px;">Retweet</p>
                                                <p style="color: #50E3C2">{{$post->retweets}}</p>
                                            </div>
                                        </td>
                                        <td style="width:30%;border-left:solid 2px #888888;padding:0 20px">
                                            <div>
                                                <p style="color: #FE9F00;margin-bottom:5px;">Reply</p>
                                                <p style="color: #FE9F00">{{$post->replies}}</p>
                                            </div>
                                        </td>
                                        @if (isset($post->replies))
                                        <td style="width:40%;border-left:solid 2px #888888;padding:0 20px">
                                            <div style="margin-right:30px;">
                                                <p style="color: #53B9EA;margin-bottom:5px;">Quote</p>
                                                <p style="color: #53B9EA">{{$post->replies}}</p>
                                            </div>
                                        </td>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                        </div>
                        @if ($i%4 === 3)
                        </div>
                        </br>
                        @endif
                    @endfor
                </div>
            </div>
        </div>
        @if(in_array('twitter', $campaign->platform))
        <div style="height:793px; padding: 20px;" class="page-bg">
            <table style="width: 100%">
                <tr>
                    <td style="text-align: left">
                        <p style="font-size:36px;font-weight:600;margin:20px 40px">Link Post
                        </p>
                    </td>
                    <td style="text-align: right">
                        <img style="margin-left:auto" height="80px" width="80px"
                            src="http://15.15.15.118:8090/img/ic_twitter.png">
                    </td>
                </tr>
            </table>
            <div style="width: 100%">
                @foreach (array_filter($post_urls, fn($i) => $i['platform'] === 'twitter') as $key => $post)
                @if($key%2 === 0)
                <div class="row">
                    @endif
                    <div class="column" style="word-wrap: break-word;">
                        <a href={{$post['url']}} style="text-decoration:none;color:black" target="_blank">
                            <p style="font-size:16px">
                                {{$post['url']}}</p>
                        </a>
                    </div>
                    @if($key%2 !== 0)
                </div>
                @endif
                @if($key === 50)
                @break
                @endif
                @endforeach
            </div>
        </div>
        @endif
        @if(in_array('facebook', $campaign->platform))
        <div style="height:793px; padding: 20px;" class="page-bg">
            <table style="width: 100%">
                <tr>
                    <td style="text-align: left">
                        <p style="font-size:36px;font-weight:600;margin:20px 40px">Link Post
                        </p>
                    </td>
                    <td style="text-align: right">
                        <img style="margin-left:auto" height="80px" width="100px"
                            src="http://15.15.15.118:8090/img/ic_facebook.png">
                    </td>
                </tr>
            </table>
            <div style="width: 100%">
                @foreach (array_filter($post_urls, fn($i) => $i['platform'] === 'facebook') as $key => $post)
                @if($key%2 === 0)
                <div class="row">
                    @endif
                    <div class="column" style="word-wrap: break-word;">
                        <a href={{$post['url']}} style="text-decoration:none;color:black" target="_blank">
                            <p style="font-size:16px">
                                {{$post['url']}}</p>
                        </a>
                    </div>
                    @if($key%2 !== 0)
                </div>
                @endif
                @if($key === 50)
                @break
                @endif
                @endforeach
            </div>
        </div>
        @endif
        @if(in_array('instagram', $campaign->platform))
        <div style="height:793px; padding: 20px;" class="page-bg">
            <table style="width: 100%">
                <tr>
                    <td style="text-align: left">
                        <p style="font-size:36px;font-weight:600;margin:20px 40px">Link Post
                        </p>
                    </td>
                    <td style="text-align: right">
                        <img style="margin-left:auto" height="80px" width="80px"
                            src="http://15.15.15.118:8090/img/ic_instagram.png">
                    </td>
                </tr>
            </table>
            <div style="width: 100%">
                @foreach (array_filter($post_urls, fn($i) => $i['platform'] === 'instagram') as $key => $post)
                @if($key%2 === 0)
                <div class="row">
                    @endif
                    <div class="column" style="word-wrap: break-word;">
                        <a href={{$post['url']}} style="text-decoration:none;color:black" target="_blank">
                            <p style="font-size:16px">
                                {{$post['url']}}</p>
                        </a>
                    </div>
                    @if($key%2 !== 0)
                </div>
                @endif
                @if($key === 50)
                @break
                @endif
                @endforeach
            </div>
        </div>
        @endif
        @if(in_array('twitter', $campaign->platform))
        <div style="height:793px; padding: 20px;" class="page-bg">
            <table style="width: 100%">
                <tr>
                    <td style="text-align: left">
                        <p style="font-size:36px;font-weight:600;margin:20px 40px">Link Robot
                        </p>
                    </td>
                    <td style="text-align: right">
                        <img style="margin-left:auto" height="80px" width="80px"
                            src="http://15.15.15.118:8090/img/ic_twitter.png">
                    </td>
                </tr>
            </table>
            <div style="width: 100%">
                @foreach ($bot_twitter_urls as $key => $bot)
                @if($key%2 === 0)
                <div class="row">
                    @endif
                    <div class="column" style="word-wrap: break-word;">
                        <a href="https://twitter.com/{{$bot['username']}}" style="text-decoration:none;color:black"
                            target="_blank">
                            <p style="font-size:16px">https://twitter.com/{{$bot['username']}}</p>
                        </a>
                    </div>
                    @if($key%2 !== 0)
                </div>
                @endif
                @if($key === 50)
                @break
                @endif
                @endforeach
            </div>
        </div>
        @endif
        @if(in_array('facebook', $campaign->platform))
        <div style="height:793px; padding: 20px;" class="page-bg">
            <table style="width: 100%">
                <tr>
                    <td style="text-align: left">
                        <p style="font-size:36px;font-weight:600;margin:20px 40px">Link Robot
                        </p>
                    </td>
                    <td style="text-align: right">
                        <img style="margin-left:auto" height="80px" width="80px"
                            src="http://15.15.15.118:8090/img/ic_facebook.png">
                    </td>
                </tr>
            </table>
            <div style="width: 100%">
                @foreach ($bot_facebook_urls as $key => $bot)
                @if($key%2 === 0)
                <div class="row">
                    @endif
                    <div class="column" style="word-wrap: break-word;">
                        <a href="https://facebook.com/{{$bot['username']}}" style="text-decoration:none;color:black"
                            target="_blank">
                            <p style="font-size:16px">https://facebook.com/{{$bot['username']}}</p>
                        </a>
                    </div>
                    @if($key%2 !== 0)
                </div>
                @endif
                @if($key === 50)
                @break
                @endif
                @endforeach
            </div>
        </div>
        @endif
        @if(in_array('instagram', $campaign->platform))
        <div style="height:793px; padding: 20px;" class="page-bg">
            <table style="width: 100%">
                <tr>
                    <td style="text-align: left">
                        <p style="font-size:36px;font-weight:600;margin:20px 40px">Link Robot
                        </p>
                    </td>
                    <td style="text-align: right">
                        <img style="margin-left:auto" height="80px" width="80px"
                            src="http://15.15.15.118:8090/img/ic_instagram.png">
                    </td>
                </tr>
            </table>
            <div style="width: 100%">
                @foreach ($bot_instagram_urls as $key => $bot)
                @if($key%2 === 0)
                <div class="row">
                    @endif
                    <div class="column" style="word-wrap: break-word;">
                        <a href="https://instagram.com/{{$bot['username']}}" style="text-decoration:none;color:black"
                            target="_blank">
                            <p style="font-size:16px">https://instagram.com/{{$bot['username']}}</p>
                        </a>
                    </div>
                    @if($key%2 !== 0)
                </div>
                @endif
                @if($key === 50)
                @break
                @endif
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <script>
        const dataJson = {!!json_encode($data->toArray())!!};

        // const bounds = [
        //     [93, -12], // Southwest coordinates
        //     [143, 9.5], // Northeast coordinates
        // ];
        // mapboxgl.accessToken =
        //     'pk.eyJ1IjoiaW50aXZhMTIzIiwiYSI6ImNrdTd1dTR6MDVvaWMydm8ydzF2NndnYWgifQ.E-RBIDvvnb6cuJyL0uKNBA';
        // const map = new mapboxgl.Map({
        //     container: 'map', // container ID
        //     style: 'mapbox://styles/intiva123/ckyo1won43k9u15qnpq6059uv',
        //     center: [-2.6856, 90.0],
        //     zoom: 1,
        //     maxBounds: bounds,
        // });

        // dataJson.analytics.maps.features.map(i => {
        //     i.geometry.coordinates.map(j => {
        //         j.map(k => {
        //             const el = document.createElement("div");
        //             el.className = "marker-yellow";
        //             const oneMarker = new mapboxgl.Marker(el)
        //                 .setLngLat([k[0],k[1]])
        //                 .addTo(map);
        //         })
        //     })
        // })
        console.log('maps', dataJson.analytics.maps)

        function getColor(d) {
            return `rgba(255, 160, 0, ${
                d / (dataJson.analytics.maps ? dataJson.analytics.maps.max : d)
            })`;
        }

        var map = L.map('map').setView([-1, 118.0], 5); //.scrollWheelZoom(false)

        L.tileLayer('https://api.mapbox.com/styles/v1/intiva123/ckyo1won43k9u15qnpq6059uv/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiaW50aXZhMTIzIiwiYSI6ImNrdTd1dTR6MDVvaWMydm8ydzF2NndnYWgifQ.E-RBIDvvnb6cuJyL0uKNBA')
        .addTo(map);

        L.geoJSON(dataJson.analytics.maps.data, {
            style: function (feature) {
                return {
                    fillColor: getColor(feature.properties.mentions),
                    weight: 2,
                    opacity: 1,
                    color: "white",
                    dashArray: "3",
                    fillOpacity: 0.7,
                };
            }
        }).bindPopup(function (layer) {
            return layer.feature.properties.description;
        }).addTo(map);

        // L.marker([51.5, -0.09]).addTo(map)
        //     .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
        //     .openPopup();

        const data = {
            labels: dataJson.analytics.timeline.map((item) => moment(item.date).format("DD MMMM YYYY")),
            datasets: [{
                label: 'My First dataset',
                backgroundColor: "rgb(83, 185, 234, 0.2)",
                borderColor: "#53B9EA",
                data: dataJson.analytics.timeline.map((item) => item.results),
                fill: true,
            }]
        };

        const config = {
            type: 'line',
            data: data,
            options: {
                maintainAspectRatio: false,
                scales: {
                    y: {
                        ticks: {
                            display: true,
                            color: "#FFFFFF"
                        },
                        grid: {
                            borderDash: [8, 4],
                            color: "rgb(216, 216, 216, 1)"
                        },
                    },
                    x: {
                        grace: 100,
                        ticks: {
                            display: true,
                            color: "#FFFFFF"
                        },
                        grid: {
                            borderDash: [8, 4],
                            color: "rgb(216, 216, 216, 1)"
                        },
                    },
                },
                plugins: {
                    title: {
                        display: false,
                    },
                    legend: {
                        display: false,
                    },
                },
            },
        };

        const myChart = new Chart(
            document.getElementById('myChart'),
            config
        );


        const cloudWords = dataJson.analytics.topics.map(item => {
            return {
                text: item[0],
                value: item[1]
            }
        })

        // set the dimensions and margins of the graph
        var margin = {
                top: 10,
                right: 10,
                bottom: 10,
                left: 10
            },
            width = 800 - margin.left - margin.right,
            height = 450 - margin.top - margin.bottom;

        // append the svg object to the body of the page
        var svg = d3.select("#my_dataviz").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")")
            .attr("text-anchor", "middle");

        var layout = d3.layout.cloud()
            .size([width, height])
            .words(cloudWords.map(function (d) {
                return {
                    text: d.text,
                    size: d.value * 0.1 + 10
                };
            }))
            .padding(5)
            .rotate(0)
            .font("Impact")
            .fontSize(function (d) {
                return d.size;
            })
            .on("end", draw);

        layout.start();

        function draw(words) {
            svg
                .append("g")
                .attr("width", layout.size()[0])
                .attr("height", layout.size()[1])
                .append("g")
                .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
                .selectAll("text")
                .data(words)
                .enter().append("text")
                .style("font-size", function (d) {
                    return d.size + "px";
                })
                .style("font-family", "Impact")
                .style("fill", function () {
                    const colors = ["#C837AB", "#03AADE", "#13C38A", "#3B8AE8", "#F64E60", "#F66B84"];
                    return colors[Math.floor(Math.random() * colors.length)];
                })
                .attr("text-anchor", "middle")
                .attr("transform", function (d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
                .text(function (d) {
                    return d.text;
                });
        }

        function reloadImage(img) {
            img.onerror = null
            img.src = "http://15.15.15.118:8090/img/sample_avatar.png"
        }

    </script>
</body>

</html>
