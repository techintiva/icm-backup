@extends('layout.main')

@section('container')
    <div class="container">
        <h2 class="text-center">Set All</h2>

        <div class="row">
            <div class="col-md-7">
                <form action="{{ route('set-all.post') }}" method="POST" class="mb-3">
                    @csrf
                    <input type="hidden" name="account_id" value="{{ json_encode($account_id) }}">
                    <input type="hidden" name="platform" value="{{ $platform }}">
                    <input type="hidden" name="status" value="{{ $status }}">
                    <div class="card-body">

                        <div>
                            <label for="active_day" class="form-label">Active Day</label>
                            <div class="row container-fluid mb-3">
                                <div class="form-check col-md-3">
                                    <input type="checkbox" class="form-check-input" id="day" name="active_day[]" value="1">
                                    <label class="form-check-label" for="day">Senin</label>
                                </div>
                                <div class="form-check col-md-3">
                                    <input type="checkbox" class="form-check-input" id="day" name="active_day[]" value="2">
                                    <label class="form-check-label" for="day">Selasa</label>
                                </div>
                                <div class="form-check col-md-3">
                                    <input type="checkbox" class="form-check-input" id="day" name="active_day[]" value="3">
                                    <label class="form-check-label" for="day">Rabu</label>
                                </div>
                                <div class="form-check col-md-3">
                                    <input type="checkbox" class="form-check-input" id="day" name="active_day[]" value="4">
                                    <label class="form-check-label" for="day">Kamis</label>
                                </div>
                                <div class="form-check col-md-3">
                                    <input type="checkbox" class="form-check-input" id="day" name="active_day[]" value="5">
                                    <label class="form-check-label" for="day">Jumat</label>
                                </div>
                                <div class="form-check col-md-3">
                                    <input type="checkbox" class="form-check-input" id="day" name="active_day[]" value="6">
                                    <label class="form-check-label" for="day">Sabtu</label>
                                </div>
                                <div class="form-check col-md-3">
                                    <input type="checkbox" class="form-check-input" id="day" name="active_day[]" value="7">
                                    <label class="form-check-label" for="day">Minggu</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-auto form-group">
                                <label for="start_hour" class="form-label">Start Hour: </label>
                                <input type="time" id="start_hour" name="start_hour">
                            </div>
                            <div class="col-auto form-group">
                                <label for="end_hour" class="form-label">End Hour: </label>
                                <input type="time" id="end_hour" name="end_hour">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="count" class="form-label">Total Auto Post</label>
                                <input type="number" class="form-control" id="count" name="count_auto_post">
                            </div>
                        </div>

                        <div>
                            <label for="active_day" class="form-label">Action</label>
                            @if ($platform == 'twitter')
                                <div class="row container-fluid mb-3">
                                    <div class="form-check col-md-3">
                                        <input type="checkbox" class="form-check-input" id="act" name="action[]"
                                            value="like">
                                        <label class="form-check-label" for="act">Like</label>
                                    </div>

                                    <div class="form-check col-md-3">
                                        <input type="checkbox" class="form-check-input" id="act" name="action[]"
                                            value="retweet">
                                        <label class="form-check-label" for="act">Retweet</label>
                                    </div>

                                    <div class="form-check col-md-3">
                                        <input type="checkbox" class="form-check-input" id="act" name="action[]"
                                            value="reply">
                                        <label class="form-check-label" for="act">Reply</label>
                                    </div>

                                    <div class="form-check col-md-3">
                                        <input type="checkbox" class="form-check-input" id="act" name="action[]"
                                            value="quote">
                                        <label class="form-check-label" for="act">Quote</label>
                                    </div>
                                </div>
                            @else
                                <div class="row container-fluid mb-3">
                                    <div class="form-check col-md-3">
                                        <input type="checkbox" class="form-check-input" id="act" name="action[]"
                                            value="like">
                                        <label class="form-check-label" for="act">Like</label>
                                    </div>

                                    <div class="form-check col-md-3">
                                        <input type="checkbox" class="form-check-input" id="act" name="action[]"
                                            value="comment">
                                        <label class="form-check-label" for="act">Comment</label>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="count" class="form-label">Total Activity</label>
                                <input type="number" class="form-control" id="count" name="count">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="follow" class="form-label">Total Follow</label>
                                <input type="number" class="form-control" id="follow" name="follow">
                            </div>
                        </div>

                        @if ($platform == 'twitter')
                            <div class="row">
                                <div class="form-group col-auto">
                                    <label for="tweet" class="form-label">Tweet</label>
                                    <input type="text" class="form-control" id="tweet" name="tweet">
                                </div>
                                <div class="form-group col-auto">
                                    <label for="retweet" class="form-label">Retweet</label>
                                    <input type="text" class="form-control" id="retweet" name="retweet">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-auto">
                                    <label for="like" class="form-label">Like</label>
                                    <input type="text" class="form-control" id="like" name="like">
                                </div>
                                <div class="form-group col-auto">
                                    <label for="reply" class="form-label">Reply</label>
                                    <input type="text" class="form-control" id="reply" name="reply">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-auto">
                                    <label for="quote" class="form-label">Quote</label>
                                    <input type="text" class="form-control" id="quote" name="quote">
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="form-group col-auto">
                                    <label for="like" class="form-label">Post</label>
                                    <input type="text" class="form-control" id="post" name="post">
                                </div>
                                <div class="form-group col-auto">
                                    <label for="like" class="form-label">Like</label>
                                    <input type="text" class="form-control" id="like" name="like">
                                </div>
                                <div class="form-group col-auto">
                                    <label for="comment" class="form-label">Comment</label>
                                    <input type="text" class="form-control" id="comment" name="comment">
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
