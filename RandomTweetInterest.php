<?php

namespace App\Services;

use App\Models\Collection\TweetInterestCollection;
//use App\Models\Adhoc\AdhocTweetCollection;

class RandomTweetInterest
{
    public function generate($interest, $type)
    {
        $tweets = TweetInterestCollection::where('interest_id', $interest)->where('type', $type)->get();
        //$tweets = AdhocTweetCollection::all();

        return (!$tweets->isEmpty()) ? $tweets->random()->caption : null;
    }
}
